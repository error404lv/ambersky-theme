<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">

	<header id="masthead" class="site-header standard-block">
        <div class="top-part">
            <div class="logo">
                <a href="<?php echo get_home_url( ); ?>"></a>
            </div>
            <div class="info-block">
                <?php
                    $email = get_field( 'main_contact_mail', 'option' );
                    $phone = get_field( 'main_contact_phone', 'option' );
                    $calendar_page = get_field( 'calendar_page', 'option' );
                ?>
                <div class="info-entry">
                    <div class="info-label">Mail us:</div>
                    <div class="info-val"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></div>
                </div>
                <div class="info-entry">
                    <div class="info-label">Call us:</div>
                    <div class="info-val"><a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a></div>
                </div>
                <div class="button orange">
                    <span>Event schedule</span>
                    <a href="<?php echo get_permalink( $calendar_page->ID ); ?>"></a>
                </div>
            </div>
        </div>
        <div class="main-menu">
            <?php
                $args = array(
                    "theme_location" => "main-menu"
                );
                wp_nav_menu($args);
            ?>
            <div class="social">
                <div class="icon-wrapper">
                    <?php get_template_part( 'template-parts/icons/icon', 'linkedin' ); ?>
                    <a href="#"></a>
                </div>
                <div class="icon-wrapper">
                    <?php get_template_part( 'template-parts/icons/icon', 'facebook' ); ?>
                    <a href="#"></a>
                </div>
            </div>
        </div>
	</header><!-- #masthead -->
