<?php

acf_add_options_page(array(
    'page_title'  => 'Web Options',
    'menu_title'  => 'Web Options',
    'menu_slug'   => 'website-options',
    'capability'  => 'edit_posts',
    'icon_url'  => 'dashicons-admin-site',
    'redirect'    => false
));


?>