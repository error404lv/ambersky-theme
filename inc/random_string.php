<?php

function generateRandomLetterString($length) {
    $letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = substr(str_shuffle($letters), 0, $length);
    return $randomString;
}