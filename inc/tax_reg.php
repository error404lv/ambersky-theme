<?php 

add_action( 'init', 'register_theme_item_cat_tax' );

function register_theme_item_cat_tax() {
    $labels = array(
        'name'              => _x( 'Entry types', 'taxonomy general name' ),
        'singular_name'     => _x( 'Entry type', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Entry types' ),
        'all_items'         => __( 'All Entry types' ),
        'parent_item'       => __( 'Parent Entry type' ),
        'parent_item_colon' => __( 'Parent Entry type:' ),
        'edit_item'         => __( 'Edit Entry type' ),
        'update_item'       => __( 'Update Entry type' ),
        'add_new_item'      => __( 'Add New Entry type' ),
        'new_item_name'     => __( 'New Entry type Name' ),
        'menu_name'         => __( 'Entry types' ),
    );
    $args   = array(
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => [ 'slug' => 'entry_type' ],
    );

    register_taxonomy( 'entry_type', [ 'item' ], $args );
}