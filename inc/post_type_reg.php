<?php


function register_theme_custom_post_types() {
    register_post_type( 'item',
        array(
            'labels' => array(
                'name' => __( 'People and partners' ),
                'singular_name' => __( 'item' )
            ),
            'public' => true,
            'show_ui' => true,
            'has_archive' => true,
            'taxonomies' => array('item-cat'),
            'menu_icon'   => 'dashicons-editor-ul',
            'supports' => array('title', 'thumbnail')
        )
    );
}

add_action( 'init', 'register_theme_custom_post_types' );