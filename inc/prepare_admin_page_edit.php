<?php 

add_filter('use_block_editor_for_post', '__return_false', 10);
add_action('init', 'remove_default_editor_from_pages');

function remove_default_editor_from_pages() {
    remove_post_type_support('page', 'editor');
}