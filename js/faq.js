jQuery('document').ready( function($) {
    $('.faq-item .question').click( function() {

        const parentBlock = $( this ).parents('.faq-item');
        if ( !parentBlock.length ) return;

        if ( ! parentBlock.hasClass( 'collapsed' ) ) {
            parentBlock.addClass('collapsed');
        } else {
            $( this ).parents( '.faq_block' ).find('.faq-item').addClass('collapsed');
            parentBlock.removeClass('collapsed');
        }
    } );
} );