function setHeaderClassOnScroll() {
    var targetElement = document.getElementById('masthead');

    // Add a scroll event listener to the window
    window.addEventListener('scroll', function() {
        // Check if the page is scrolled to the top
        if (window.scrollY > 0) {
            // Add the class to the target element
            targetElement.classList.add('your-class-name');
        } else {
            // Remove the class from the target element
            targetElement.classList.remove('your-class-name');
        }
    });
}
