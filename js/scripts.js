
setHeaderClassOnScroll();

jQuery(document).ready( function($) {
    //calendar

    $('.date-opt').click( function() {
        if ($(this).hasClass('active')) return;

        const index = $(this).index();
        
        $('.day-entry.active').removeClass('active');
        $('.date-opt.active').removeClass('active');
        
        $('.day-entry').eq(index).addClass('active')
        $(this).addClass('active');

    } );

} );

