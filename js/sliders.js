const swiper = new Swiper('.main-slider', {
    // Optional parameters
    loop: true,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});

jQuery(document).ready(function($) {

    const sliders = $('.multiple-items.swiper');
    sliders.each(function() {
        const id = this.id;
        const blockID = '#' + id;
        const itemsPerView = Number(this.dataset.perview);
        const theSwiper = new Swiper(blockID, {
            // Optional parameters
            //loop: true,
            slidesPerView: itemsPerView,
            autoHeight: true,
            spaceBetween: 20,
            // If we need pagination
            pagination: {
                el: blockID + ' .swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: blockID + ' .swiper-button-next',
                prevEl: blockID + ' .swiper-button-prev',
            },
        });
    });

    const galleries = $('.gallery-block.swiper');
    galleries.each(function() {
        const id = this.id;
        const blockID = '#' + id;
        const theSwiper = new Swiper(blockID, {
            // Optional parameters
            //loop: true,
            slidesPerView: 2.5,
            autoHeight: true,
            spaceBetween: 0,

            // Navigation arrows
            navigation: {
                nextEl: blockID + ' .swiper-button-next',
                prevEl: blockID + ' .swiper-button-prev',
            },
        });
    });
});

jQuery(document).ready(function($) {
    $('.slder-with-bg-wrapper .bulletpoint').click( function() {
        const itemIndex = $(this).index();
        const parent = $(this).parents('.slder-with-bg-wrapper');
        if ( $(this).hasClass('active') ) return;

        console.log(itemIndex);

        parent.find('.bulletpoint.active').removeClass('active');
        parent.find('.slide-wrapper.active').removeClass('active');
        parent.find('.background.active').removeClass('active');
        $(this).addClass('active');
        parent.find('.slide-wrapper').eq(itemIndex).addClass('active');
        parent.find('.background').eq(itemIndex).addClass('active');
        
    } )
});

