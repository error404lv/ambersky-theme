<?php
    $data = $args['data'];
    $index = 0;
    $index2 = 0;
?>

<div class="calendar-wrapper wide-block page-block">

    <div class="date-selector">
        <?php foreach ( $data as $date ) : ?>
            <?php 
                $class = "date-opt";
                $class .= !$index ? ' active' : '';
            ?>
            <div class="<?php echo $class; ?>"><?php echo $date['date'];?></div>
            <?php $index++; ?>
        <?php endforeach; ?>
    </div>

    <div class="event-contents standard-block">
        <?php foreach ( $data as $date ) : ?>
            <?php 
                $class = "day-entry";
                $class .= !$index2 ? ' active' : '';
            ?>
            <div class="<?php echo $class; ?>">
                <?php foreach ( $date["event_entries"] as $event ) : ?>
                    <div class="event-entry">
                        <div class="time"><?php echo $event['time']; ?></div>
                        <div class="event-info">
                            <div class="title"><?php echo $event['title']; ?></div>
                            <div class="description"><?php echo $event['description']; ?></div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php $index2++; ?>
        <?php endforeach; ?>
    </div>

</div>