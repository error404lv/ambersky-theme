<?php
    $data = $args['data'];
    
    $container_classes = 'page-block standard-block multiple-items';
    $container_classes .= $data['layout'] == 'grid' ? ' grid' : ' swiper';
    $container_classes .= ' items-' . $data['items_per_line'];
    $container_classes .= $data['items_has_background'] ? ' with-background' : '';

    $wrapper_class = $data['layout'] == 'grid' ? 'grid-wrapper items-' . $data['items_per_line']  : ' swiper-wrapper';

    $slide_wrapper_class = $data['layout'] == 'grid' ? 'grid-entry-wrapper entry-wrapper' : 'entry-wrapper swiper-slide';
    $slide_class = 'entry';
?>


<div class="<?php echo $container_classes; ?>" id="<?php echo generateRandomLetterString(5); ?>" data-perview="<?php echo $data['items_per_line']; ?>">
    <!-- Additional required wrapper -->
    <div class="<?php echo $wrapper_class; ?>">
        <!-- Slides -->
        <?php foreach ( $data['items'] as $item ) : ?>
            <div class="<?php echo $slide_wrapper_class; ?>">
                <div class="<?php echo $slide_class; ?>">
                    <div class="item-image" style="background-image: url(<?php echo $item['image']['sizes']['large']; ?>);"></div>
                    <div class="item-data">
                        <p class="item-title"><?php echo $item['title']; ?></p>
                        <p class="item-content"><?php echo $item['content']; ?></p>
                        <div class="button">
                            <span><?php echo $item['button']['button_text']; ?></span>
                            <a href="<?php echo $item['button']['button_link']; ?>"></a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <?php if ( $data['layout'] == 'slider') : ?>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    <?php endif; ?>
</div>