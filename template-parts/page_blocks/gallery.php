<?php
    $data = $args['data'];
    $gallery_slides = array_chunk($data['gallery_images'], 3);
    $counter = 1;
?>

<div class="gallery-block wide-block page-block swiper" id="<?php echo generateRandomLetterString(5); ?>">
    <div class="gallery-wrapper swiper-wrapper">
        <?php foreach ($gallery_slides as $slide) : ?>
            <?php
                $slide_classes = 'gallery-slide';
                $slide_classes .= ' items-' . count($slide);
                $slide_classes .= ' counter-' . $counter;
            ?>
            <div class="swiper-slide">
                <div class="<?php echo $slide_classes; ?>">
                    <?php foreach ( $slide as $img ) : ?>
                        <div class="slide-img-wrapper">
                            <div class="slide-img" style="background-image:url(<?php echo $img['sizes']['large']; ?>);">
                                <a data-fslightbox href="<?php echo $img['sizes']['large']; ?>"></a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php
                $counter = $counter == 2 ? 1 : $counter + 1;
            ?>
        <?php endforeach; ?>
    </div>
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
</div>