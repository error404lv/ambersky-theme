<?php
    $data = $args['data'];
    $img = isset($data['image']) ? $data['image']['sizes']['large'] : false;
    $block_classes = 'page-block standard-block quote_block';
    $block_classes .= $img ? ' has-img' : '';
?>

<?php if ($data['has_background']) : ?>
<div class="page-block-background">
<?php endif; ?>
    <div class="<?php echo $block_classes; ?>">
        <?php if ($img) : ?>
        <div class="image" style="background-image: url(<?php echo $img; ?>);"></div>
        <?php endif; ?>
        <div class="the-quote">
            <div class="content"><?php echo $data['quote']; ?></div>
            <div class="author"><?php echo $data['quote_author']; ?></div>
            <div class="author-position"><?php echo $data['quote_author_position']; ?></div>
        </div>
    </div>
<?php if ($data['has_background']) : ?>
</div>
<?php endif; ?>