<?php
    $data = $args['data'];
    $block_classes = 'page-block standard-block form_block';
    $block_classes .= ' ' . $data['theme'] . '-theme';
    $block_classes .= ' ' . $data['left_part_content_type'] . '-layout';

    if ( $data['left_part_content_type'] == 'image' ) {
        $img = $data['image']['sizes']['large'];
    }

?>

<div class="<?php echo $block_classes; ?>">
    <div class="left-block form-block-col">
        
        <?php if ( $data['left_part_content_type'] == 'image' ) : ?>
        <div class="image" style="background-image: url(<?php echo $img; ?>);"></div>
        <?php endif; ?>
        
        <?php if ( $data['left_part_content_type'] == 'text' ) : ?>
            <div class="form-text-content">
                <div class="title"><?php echo $data['text_content']['title']; ?></div>
                <div class="description"><?php echo $data['text_content']['description']; ?></div>
            </div>
        <?php endif; ?>

    </div>
    <div class="right-block form-block-col">
        <?php echo do_shortcode( $data['form_shortcode'] ); ?>
    </div>
</div>