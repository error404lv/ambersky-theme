<?php
    $data = $args['data'];
    $content = apply_filters( 'the_content', $data['content'] );
?>

<?php if ($data['has_background']) : ?>
<div class="page-block-background">
<?php endif; ?>
    <div class="page-block standard-block text_block">
        <?php echo $content; ?>
    </div>
<?php if ($data['has_background']) : ?>
</div>
<?php endif; ?>