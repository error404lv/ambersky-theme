<?php
   $data = $args['data'];
   $block_classes = 'page-block icon_block';
   $block_classes .= ' ' . $data['theme'] . '-theme';
   $block_classes .= ' ' . $data['layout'] . '-layout';
   $block_classes .= $data['layout'] == 'grid' ? ' standard-block' : ' wide-block';
   $block_classes .= ' items-' . $data['items_per_row'];

   $index = 0;
   $items_per_row = intval( $data['items_per_row'] );
?>

<div class="icon-block-wrapper <?php echo $data['theme']; ?>">
    <div class="<?php echo $block_classes; ?>">
        <?php foreach( $data['items'] as $item ) : ?>
            <?php if ( $data['layout'] != 'strip' || $index < $items_per_row ) : ?>
                <?php $index++; ?>
                <div class="item-wrapper">
                    <div class="item">
                        <div class="icon">
                            <?php get_template_part( 'template-parts/icons/icon', $item['icon_code'] ); ?>
                        </div>
                        <div class="data">
                            <div class="title"><?php echo $item['title']; ?></div>
                            <div class="description"><?php echo $item['content']; ?></div>
                        </div>
                        <?php if ( $item['link'] ) : ?>
                            <a href="<?php echo $item['link']; ?>"></a>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>