<?php

    $data = $args['data'];
    $i = 0;
?>

<div class="page-block standard-block faq_block">
    <?php foreach ( $data['items'] as $item ) : ?>
        <?php 
            $item_class = "faq-item";
            $item_class .= !$i ? '' : ' collapsed';
        ?>
        <div class="<?php echo $item_class; ?>">
            <div class="question"><?php echo $item['question']; ?></div>
            <div class="answer"><?php echo $item['answer']; ?></div>
        </div>
        <?php $i++; ?>
    <?php endforeach; ?>
</div>