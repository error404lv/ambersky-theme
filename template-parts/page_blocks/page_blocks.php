<?php

$blocks = get_fields();

if ( isset( $blocks['blocks'] ) && $blocks['blocks'] && count($blocks['blocks']) ) :
    foreach( $blocks['blocks'] as $block ) :
        
        if ( $block['acf_fc_layout'] == 'text_block' ) {
            get_template_part( 'template-parts/page_blocks/text_block', null, array( 'data' => $block ) );
        }

        if ( $block['acf_fc_layout'] == 'multiple_items' ) {
            get_template_part( 'template-parts/page_blocks/multiple_items', null, array( 'data' => $block ) );
        }
    
        if ( $block['acf_fc_layout'] == 'quote' ) {
            get_template_part( 'template-parts/page_blocks/quote', null, array( 'data' => $block ) );
        }
        
        if ( $block['acf_fc_layout'] == 'faq' ) {
            get_template_part( 'template-parts/page_blocks/faq', null, array( 'data' => $block ) );
        }
    
        if ( $block['acf_fc_layout'] == 'form' ) {
            get_template_part( 'template-parts/page_blocks/form', null, array( 'data' => $block ) );
        }

        if ( $block['acf_fc_layout'] == 'icon_block' ) {
            get_template_part( 'template-parts/page_blocks/icon_block', null, array( 'data' => $block ) );
        }
    
        if ( $block['acf_fc_layout'] == 'slider_with_background' ) {
            get_template_part( 'template-parts/page_blocks/slider_with_background', null, array( 'data' => $block ) );
        }
    
        if ( $block['acf_fc_layout'] == 'banners' ) {
            get_template_part( 'template-parts/page_blocks/banners', null, array( 'data' => $block ) );
        }
    
        if ( $block['acf_fc_layout'] == 'gallery' ) {
            get_template_part( 'template-parts/page_blocks/gallery', null, array( 'data' => $block ) );
        }
    
        if ( $block['acf_fc_layout'] == 'text_with_image' ) {
            get_template_part( 'template-parts/page_blocks/text_with_image', null, array( 'data' => $block ) );
        }
    
        if ( $block['acf_fc_layout'] == 'entry_list' ) {
            get_template_part( 'template-parts/page_blocks/entry_list', null, array( 'data' => $block ) );
        }
    
    endforeach;
endif;

?>