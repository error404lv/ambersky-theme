<?php
    $data = $args['data'];
    $index0 = 0;
    $index = 0;
    $index2 = 0;
    
?>

<div class="slder-with-bg-wrapper wide-block page-block">
    <div class="backgrounds-wrapper">
        <?php foreach ( $data['slides'] as $slide ) : ?>
            <?php
                $class = "background";
                $class .= !$index0 ? ' active': '';
            ?>
            <div class="<?php echo $class; ?>" style="background-image:url(<?php echo $slide['background_image']['sizes']['large']; ?>)" ></div>
            <?php $index0++; ?>
        <?php endforeach; ?>
    </div>
    <div class="standard-block">
        <div class="slides-content-wrapper">
            <?php foreach ( $data['slides'] as $slide ) : ?>
                <?php
                    $class = "slide-wrapper";
                    $class .= !$index ? ' active': '';
                ?>
                <div class="<?php echo $class; ?>">
                    <div class="slide">
                        <div class="title"><?php echo $slide['title']; ?></div>
                        <div class="description"><?php echo $slide['content']; ?></div>
                    </div>
                </div>
                <?php $index++; ?>
            <?php endforeach; ?>
            <div class="bullets">
                <?php foreach ( $data['slides'] as $slide ) : ?>
                    <?php 
                        $class = "bulletpoint";
                        $class .= !$index2 ? ' active' : '';
                    ?>
                    <div class="<?php echo $class; ?>"></div>
                    <?php $index2++; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>