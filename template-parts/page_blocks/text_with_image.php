<?php
    $data = $args['data'];
    $content = apply_filters( 'the_content', $data['content'] );

    $classes = 'page-block standard-block text_with_image-block';
    $classes .= ' img-' . $data['image_position'];
?>

<?php if ($data['has_background']) : ?>
<div class="page-block-background">
<?php endif; ?>
    <div class="<?php echo $classes; ?>">
        <div class="image-wrapper">
            <div class="image" style="background-image:url(<?php echo $data['image']['sizes']['large']; ?>)"></div>
        </div>
        <div class="content-wrapper">
            <?php echo $content; ?>
        </div>
    </div>
<?php if ($data['has_background']) : ?>
</div>
<?php endif; ?>