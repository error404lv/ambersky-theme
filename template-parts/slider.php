<?php

    $data = get_fields();
    $slider = $data['slider'];

    if ( $slider && count( $slider ) ) : 

?>

<div class="swiper wide-block main-slider">
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">
        <!-- Slides -->
        <?php foreach ($slider as $slide) : ?>
        <div class="swiper-slide slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/tmp/plane.png);">
            <div class="slide-content standard-block">
                <div class="slide-content-top">
                    <p class="slide-header"><?php echo $slide['title']; ?></p>
                    <p class="slide-subheader"><?php echo $slide['subtitle']; ?></p>
                </div>
                <div class="slide-content-body"><?php echo $slide['description']; ?></div>
                <?php if ( $slide['button']['button_text'] && $slide['button']['button_link'] ) : ?>
                    <div class="button">
                        <span><?php echo $slide['button']['button_text']; ?></span>
                        <a href="<?php echo $slide['button']['button_link']; ?>"></a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
    <!-- If we need pagination -->
    <div class="swiper-pagination standard-block"></div>

    <!-- If we need navigation buttons -->
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>

</div>

<?php endif; ?>