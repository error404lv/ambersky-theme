<?php /* Template Name: Index */ ?>

<?php
get_header();
?>

	<main id="primary" class="site-main">
        <?php get_template_part( 'template-parts/slider' ); ?>

        <?php
		while ( have_posts() ) :
			the_post();

			require_once('template-parts/page_blocks/page_blocks.php');

		endwhile; // End of the loop.
		?>
		

	</main><!-- #main -->

<?php
get_footer();