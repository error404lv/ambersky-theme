<?php

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

function ambersky_setup() {

	add_theme_support( 'post-thumbnails' );

	register_nav_menus(
		array(
			'main-menu' => esc_html__( 'Main menu', 'ambersky' ),
			'footer-content' => esc_html__( 'Footer content', 'ambersky' ),
			'footer-legal' => esc_html__( 'Footer legal', 'ambersky' ),
			'footer-information' => esc_html__( 'Footer information', 'ambersky' ),
		)
	);

	add_theme_support(
		'html5',
		array(
			'gallery',
			'caption',
			'style',
			'script',
		)
	);
}
add_action( 'after_setup_theme', 'ambersky_setup' );


function ambersky_scripts() {
	wp_enqueue_style( 'ambersky-css', get_stylesheet_uri(), array(), _S_VERSION );
    wp_enqueue_style( 'swiper-css', get_template_directory_uri() . '/js/dist/swiper-bundle.min.css', array(), _S_VERSION );
    wp_enqueue_style( 'font-css', 'https://fonts.googleapis.com/css2?family=Rubik:wght@400;700&display=swap', array(), _S_VERSION );
	wp_style_add_data( 'ambersky-style', 'rtl', 'replace' );

	wp_enqueue_script( 'swiper-js', get_template_directory_uri() . '/js/dist/swiper-bundle.min.js', array(), _S_VERSION, true );
    wp_enqueue_script( 'lightbox-js', get_template_directory_uri() . '/js/dist/lightbox/fslightbox.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'header-js', get_template_directory_uri() . '/js/header.js', array('jquery'), _S_VERSION, true );
	wp_enqueue_script( 'faq-js', get_template_directory_uri() . '/js/faq.js', array('jquery'), _S_VERSION, true );
	wp_enqueue_script( 'sliders-js', get_template_directory_uri() . '/js/sliders.js', array('jquery', 'swiper-js'), _S_VERSION, true );
	wp_enqueue_script( 'scripts-js', get_template_directory_uri() . '/js/scripts.js', array('jquery', 'header-js', 'sliders-js', 'lightbox-js'), _S_VERSION, true );

    
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'ambersky_scripts' );

require_once('inc/random_string.php');
require_once('inc/prepare_admin_page_edit.php');
require_once('inc/add_options_page.php');
require_once('inc/post_type_reg.php');
require_once('inc/tax_reg.php');

