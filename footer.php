<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ambersky
 */

?>

	<footer id="colophon" class="site-footer">
        <div class="footer-content standard-block">
            <div class="menu-wrapper">
                <div class="footer-menu-col">
                    <div class="footer-menu-title">CONTENT</div>
                    <?php
                        $args = array(
                            "theme_location" => "footer-content"
                        );
                        wp_nav_menu($args);
                    ?>
                </div>
                <div class="footer-menu-col">
                    <div class="footer-menu-title">LEGAL</div>
                    <?php
                        $args = array(
                            "theme_location" => "footer-legal"
                        );
                        wp_nav_menu($args);
                    ?>
                </div>
                <div class="footer-menu-col">
                    <div class="footer-menu-title">INFORMATION</div>
                    <?php
                        $args = array(
                            "theme_location" => "footer-information"
                        );
                        wp_nav_menu($args);
                    ?>
                </div>
            </div>
            <div class="copyright">Copyright © 2023 AmberSky. All Rights Reserved.</div>
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
