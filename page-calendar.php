<?php /* Template Name: Event calendar */ ?>

<?php
get_header();
?>

	<main id="primary" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

            $cal_data = get_fields();
            
            get_template_part( 'template-parts/calendar', null, array( "data" => $cal_data['days'] ) );

			require_once('template-parts/page_blocks/page_blocks.php');

		endwhile; // End of the loop.
		?>

	</main><!-- #main -->

<?php
get_footer();